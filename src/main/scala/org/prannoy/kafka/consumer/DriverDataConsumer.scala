package org.prannoy.kafka.consumer

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.prannoy.util.{ConfigManager, SparkConfBuilder, Utility}


object DriverDataConsumer {

  def main(args: Array[String]): Unit = {

Utility.setLogger()

    val ssc = new StreamingContext(SparkConfBuilder.getSparkDriverConsumerConf(),
      Seconds(ConfigManager.getInt("spark.driver.consumer.streaming.time")
      ))

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> ConfigManager.getString("kafka.bootstrap.servers"),
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> ConfigManager.getString("kafka.auto.offset.reset"),
      "enable.auto.commit" -> ConfigManager.getString("kafka.enable.auto.commit"),
      "group.id" -> ConfigManager.getString("kafka.driver.groupid")

    )

    val topics = Array(ConfigManager.getString("kafka.driver.topic"))
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )
    stream.map(record => (record.value))
      .saveAsTextFiles(ConfigManager.getString("spark.driver.write.directory")
        , ConfigManager.getString("spark.driver.write.suffix"))

    ssc.start()
    ssc.awaitTermination()

  }
}

