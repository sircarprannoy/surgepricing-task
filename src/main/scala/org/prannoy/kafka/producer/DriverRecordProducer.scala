  package org.prannoy.kafka.producer

  import java.time.Instant
  import java.util.Properties
  import java.util.concurrent.TimeUnit

  import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
  import org.prannoy.util._

  object DriverRecordProducer {
    Utility.setLogger()
    def main(args: Array[String]): Unit = {

      val driverTopic = ConfigManager.getString("kafka.driver.topic")
      val driverRecordproducer: KafkaProducer[Integer, String]
      = new KafkaProducer[Integer, String](getDriverProducerProperties())
      while(true) {
        val timeStamp : Long = Instant.now.getEpochSecond
        for (driverNumber <- 1 to Constants.TOTAL_NUMBER_OF_DRIVERS){
          val recordLine: String = DataGenerator.generateDriverRecordLine(timeStamp,driverNumber)
          driverRecordproducer
            .send(new ProducerRecord[Integer, String](driverTopic,  recordLine))
        }
        TimeUnit.SECONDS.sleep(Constants.TEN)
      }
    }

    def getDriverProducerProperties(): Properties ={
      val  producerProperties: Properties = new Properties()
      producerProperties.put("bootstrap.servers", ConfigManager.getString("kafka.bootstrap.servers"))
      producerProperties.put("key.serializer", ConfigManager.getString("kafka.key.serializer"))
      producerProperties.put("value.serializer", ConfigManager.getString("kafka.value.serializer"))
      producerProperties
    }

  }
