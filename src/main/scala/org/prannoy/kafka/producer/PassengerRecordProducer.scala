package org.prannoy.kafka.producer

import java.time.Instant
import java.util.Properties
import java.util.concurrent.TimeUnit

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.prannoy.util._

object PassengerRecordProducer {
  Utility.setLogger()
  def main(args: Array[String]): Unit = {

    val passengerTopic = ConfigManager.getString("kafka.passenger.topic")
    val passengerRecordproducer: KafkaProducer[Integer, String]
    = new KafkaProducer[Integer, String](getPassengerProducerProperties())
    while(true) {
      val timeStamp : Long = Instant.now.getEpochSecond
      for (passengerNumber <- 1 to Utility.getRandomNumber(1,Constants.MAX_PASSENGER_LIMIT)){
        val recordLine: String = DataGenerator
          .generatePassengerRecordLine(timeStamp,passengerNumber)
        passengerRecordproducer
          .send(new ProducerRecord[Integer, String](passengerTopic,  recordLine))
      }
      TimeUnit.SECONDS.sleep(Constants.TEN)
    }
  }

  def getPassengerProducerProperties(): Properties ={
    val  producerProperties: Properties = new Properties()
    producerProperties.put("bootstrap.servers", ConfigManager.getString("kafka.bootstrap.servers"))
    producerProperties.put("key.serializer", ConfigManager.getString("kafka.key.serializer"))
    producerProperties.put("value.serializer", ConfigManager.getString("kafka.value.serializer"))
    producerProperties
  }

}
