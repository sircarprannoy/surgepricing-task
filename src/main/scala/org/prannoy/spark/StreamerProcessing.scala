package org.prannoy.spark

import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.prannoy.util._

object StreamerProcessing {
  Utility.setLogger()
  case class DRIVER (timeStamp : Long,
                     driverId : String,
                     latitude : String,
                     longitude : String,
                     geohash : String
                    )

  case class PASSENGER (timeStamp : Long,
                        passengerId : String,
                     latitude : String,
                     longitude : String,
                     geohash : String
                    )

  def main(args: Array[String]): Unit = {


    val ssc = new StreamingContext(SparkConfBuilder.getSparkStreamProcessingConf(),
      Seconds(ConfigManager.getInt("spark.streamer.streaming.time")))

    val driverDataStream = ssc.textFileStream(ConfigManager.getString("driver.data.stream.dir"))
    val passengerDataStream = ssc.textFileStream(ConfigManager.getString("passenger.data.stream.dir"))

    val driverAvailabilityStream = driverDataStream.map(recordLine => {
      val geohash = recordLine.split(",")(4)
      (geohash,1)
    })
      .reduceByKey((x,y) => x+y)


    val passengerAvailabilityStream = passengerDataStream.map(recordLine => {
      val geohash = recordLine.split(Constants.DELIMETER)(4)
      (geohash,1)
    })
      .reduceByKey((x,y) => x+y)

    val demandAndSupply = driverAvailabilityStream.leftOuterJoin(passengerAvailabilityStream)

    val trafficCongestion = driverDataStream.map(recordLine => {
      val columns = recordLine.split(Constants.DELIMETER)
      val timestamp = columns(0)
      val id = columns(1)
      val latitude = columns(2)
      val longitude = columns(3)
      val geohash = columns(4)
      ((id,geohash), Seq(Tuple3(latitude,longitude,timestamp)))
    })
      .reduceByKey((x,y) => x++y)
      .map(x => {
        val geohash = x._1._2
        val speed = getSpeed(x._2)
        (geohash,Seq(speed))
      })
      .reduceByKey((x,y) => x++y)
      .map(x => {
        val numberOfSpeedSample = x._2.size
        val sumOfSpeed = x._2.sum
        val averageSpeed = sumOfSpeed/numberOfSpeedSample
        (x._1,averageSpeed)
      })

    val weatherStats = driverDataStream.map(recordLine => {
      val columns = recordLine.split(Constants.DELIMETER)
      val timestamp = columns(0)
      val geohash = columns(4)
      val weatherDetail = DataGenerator.generateWeatherRecordLine(geohash,timestamp.toLong)
      (geohash,weatherDetail)
    }).reduceByKey((x,y) => x)
        val joinedRecord = demandAndSupply.leftOuterJoin(trafficCongestion).leftOuterJoin(weatherStats)


    ssc.start()
    ssc.awaitTermination()
  }

  def getSpeed(list : Seq[Tuple3[String, String, String]]): Double = {
    list.sortBy(x => x._3.toDouble)
    val startTimestamp = list.head._3.toDouble
    val endTimestamp = list.reverse.head._3.toDouble
    val startingPointLatitude = list.head._1.toDouble
    val startingPointLongitude = list.head._2.toDouble
    val endPointLatitude = list.reverse.head._1.toDouble
    val endPointLongitude = list.reverse.head._2.toDouble

    val netDistanceTravelled = Utility.distance(
      startingPointLatitude,
      startingPointLongitude,
      endPointLatitude,
      endPointLongitude,
      "K")
    val netTimeTaken = endTimestamp-startTimestamp
    val speed: Double = netDistanceTravelled/netTimeTaken

    speed
  }

}
