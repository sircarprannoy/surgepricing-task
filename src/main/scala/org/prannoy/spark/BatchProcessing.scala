package org.prannoy.spark

import java.io._
import java.time.Instant
import java.util._

import org.apache.spark.{SparkConf, SparkContext}
import org.prannoy.spark.StreamerProcessing.getSpeed
import org.prannoy.util.{ConfigManager, Constants, DataGenerator, SparkConfBuilder}

object BatchProcessing {

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(SparkConfBuilder.getSparkBatchProcessingConf())

    val driverData = sc.textFile(getListOfDirectoriesToProcess(1, Constants.DRIVER_TAG))
    val passengerData = sc.textFile(getListOfDirectoriesToProcess(1,Constants.PASSENGER_TAG))

    val driverAvailability = driverData.map(recordLine => {
      val geohash = recordLine.split(",")(4)
      (geohash,1)
    })
      .reduceByKey((x,y) => x+y)

    val passengerAvailability = passengerData.map(recordLine => {
      val geohash = recordLine.split(Constants.DELIMETER)(4)
      (geohash,1)
    })
      .reduceByKey((x,y) => x+y)

    val demandAndSupply = driverAvailability.leftOuterJoin(passengerAvailability)

    val trafficCongestion = driverData.map(recordLine => {
      val columns = recordLine.split(Constants.DELIMETER)
      val timestamp = columns(0)
      val id = columns(1)
      val latitude = columns(2)
      val longitude = columns(3)
      val geohash = columns(4)
      ((id,geohash), Seq(Tuple3(latitude,longitude,timestamp)))
    })
      .reduceByKey((x,y) => x++y)
      .map(x => {
        val geohash = x._1._2
        val speed = getSpeed(x._2)
        (geohash,Seq(speed))
      })
      .reduceByKey((x,y) => x++y)
      .map(x => {
        val numberOfSpeedSample = x._2.size
        val sumOfSpeed = x._2.sum
        val averageSpeed = sumOfSpeed/numberOfSpeedSample
        (x._1,averageSpeed)
      })

    val weatherStats = driverData.map(recordLine => {
      val columns = recordLine.split(Constants.DELIMETER)
      val timestamp = columns(0)
      val geohash = columns(4)
      val weatherDetail = DataGenerator.generateWeatherRecordLine(geohash,timestamp.toLong)
      (geohash,weatherDetail)
    }).reduceByKey((x,y) => x)
    val joinedRecord = demandAndSupply.leftOuterJoin(trafficCongestion).leftOuterJoin(weatherStats)
  }

  def getListOfDirectoriesToProcess(numberOfHoursToProcess : Int, pathType : String): String ={
    var basePath = ""
    if (pathType.equals(Constants.DRIVER_TAG))
    basePath = ConfigManager.getString("spark.batch.driver.data.dir")
    if (pathType.equals(Constants.PASSENGER_TAG))
    basePath = ConfigManager.getString("spark.batch.passenger.data.dir")

    val folder = new File(basePath)
    val currentTimeStamp : Long = Instant.now.getEpochSecond
    val startTimeStamp = currentTimeStamp - (ConfigManager.getInt("spark.batch.time")*3600)
    val listOfFiles = folder.listFiles()
    val filteredListOfFiles: Array[String] = listOfFiles
      .filter(file => new Date(file.lastModified()).getTime > startTimeStamp)
      .map(file => basePath + file.getName)
    filteredListOfFiles.mkString(Constants.DELIMETER)
  }

}
