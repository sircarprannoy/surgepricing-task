package org.prannoy.util

object Constants {

  val TEN : Int = 10
  val MAX_PASSENGER_LIMIT = 10000
  val LAT_LON_START_BUFFER_RANGER : Double = 0.002
  val LAT_LON_END_BUFFER_RANGER : Double = 0.005
  val GEO_HASH_GRID_SIZE : Int = 6
  val TOTAL_NUMBER_OF_DRIVERS : Int = 1000
  val DRIVER_TAG = "DRIVER"
  val PASSENGER_TAG = "PASSENGER"
  val SEPARATOR = "_"
  val DELIMETER = ","

}
