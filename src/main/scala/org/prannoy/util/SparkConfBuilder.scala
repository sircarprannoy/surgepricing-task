package org.prannoy.util

import org.apache.spark.SparkConf

object SparkConfBuilder {

  def getSparkDriverConsumerConf(): SparkConf = {
    val conf = new SparkConf()
      .setMaster(ConfigManager.getString("spark.master"))
      .setAppName(ConfigManager.getString("spark.driver.consumer.appname"))
      .set("spark.driver.memory", ConfigManager.getString("spark.driver.consumer.memory"))
    conf
  }

  def getSparkPassengerConsumerConf(): SparkConf ={
    val conf = new SparkConf()
      .setMaster(ConfigManager.getString("spark.master"))
      .setAppName(ConfigManager.getString("spark.passenger.consumer.appname"))
      .set("spark.driver.memory", ConfigManager.getString("spark.passenger.consumer.memory"))
    conf
  }

  def getSparkStreamProcessingConf(): SparkConf ={
    val conf = new SparkConf()
      .setMaster(ConfigManager.getString("spark.master"))
      .setAppName(ConfigManager.getString("spark.streamer.appname"))
      .set("spark.driver.memory", ConfigManager.getString("spark.streamer.memory"))
    conf
  }

  def getSparkBatchProcessingConf(): SparkConf ={
    val conf = new SparkConf()
      .setMaster(ConfigManager.getString("spark.master"))
      .setAppName(ConfigManager.getString("spark.batch.appname"))
      .set("spark.driver.memory", ConfigManager.getString("spark.batch.memory"))
    conf
  }

}
