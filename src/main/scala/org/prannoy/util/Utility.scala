package org.prannoy.util

import java.io.{File, PrintWriter}

object Utility {

  def getRandomNumber (start: Int, end : Int): Int ={
    val random = new scala.util.Random
    start + random.nextInt( (end - start) + 1 )
  }

  def getRandomNumber (start: Double, end : Double): Double ={
    (Math.random * ((end - start) + 1)) + start
  }

  def writeToFile(data : String, filePath : String): Unit ={
    val pw = new PrintWriter(new File(filePath ))
    pw.write(data)
    pw.close
  }

  def setLogger(): Unit ={
    import org.apache.log4j.{Level, Logger}
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    Logger.getLogger("info").setLevel(Level.OFF)

  }

  def distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: String) = {
    val theta = lon1 - lon2
    var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist = dist * 60 * 1.1515
    if (unit eq "K") dist = dist * 1.609344
    else if (unit eq "N") dist = dist * 0.8684
    dist
  }

  def deg2rad(deg: Double) = deg * Math.PI / 180.0

  def rad2deg(rad: Double) = rad * 180 / Math.PI

}
