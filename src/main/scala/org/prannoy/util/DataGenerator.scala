package org.prannoy.util

import java.time.Instant

import ch.hsr.geohash.GeoHash
import org.joda.time.DateTime

object DataGenerator {
  case class LatitudeLongitude (latitude : Double, longitude : Double)
  val latitudeLongitudeMap: Map[Int, LatitudeLongitude] = Map(
    1 -> LatitudeLongitude(51.244664, -6.14363),
    2 -> LatitudeLongitude(52.244665, -6.14083),
    3 -> LatitudeLongitude(53.244667, -6.14523),
    4 -> LatitudeLongitude(54.244669, -6.24053),
    5 -> LatitudeLongitude(55.244671, -6.11053),
    6 -> LatitudeLongitude(56.244673, -6.14553),
    7 -> LatitudeLongitude(57.244675, -6.15586),
    8 -> LatitudeLongitude(58.244677, -6.15629),
    9 -> LatitudeLongitude(59.244679, -6.15672),
    10 -> LatitudeLongitude(60.244681, -6.15710)
  )

  val weatherDataMap: Map[Int,String] = Map(
    0 -> "0:00,64  F,64  F,100 %,S,22 mph,29 mph,29.6 in,0.0 in,0.0 in,Cloudy / Windy",
    1 -> "1:00,63  F,63  F,100 %,S,22 mph,28 mph,29.6 in,0.0 in,0.0 in,Cloudy / Windy",
    2 -> "2:00,63  F,63  F,100 %,SSW,22 mph,31 mph,29.6 in,0.0 in,0.0 in,Light Rain / Windy",
    3 -> "3:00,62  F,62  F,100 %,NNW,16 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    4 -> "4:00,60  F,60  F,100 %,N,14 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    5 -> "5:00,59  F,58  F,96 %,NNE,7 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    6 -> "6:00,58  F,58  F,100 %,NNE,7 mph,0 mph,29.6 in,0.1 in,0.0 in,Heavy Rain",
    7 -> "7:00,59  F,59  F,100 %,ESE,7 mph,0 mph,29.6 in,0.1 in,0.0 in,Rain",
    8 -> "8:00,60  F,60  F,100 %,ESE,5 mph,0 mph,29.6 in,0.0 in,0.0 in,Mostly Cloudy",
    9 -> "9:00,61  F,61  F,100 %,S,14 mph,0 mph,29.6 in,0.0 in,0.3 in,Mostly Cloudy",
    10 -> "10:00,60  F,60  F,100 %,WSW,20 mph,26 mph,29.6 in,0.0 in,0.0 in,Cloudy",
    11 -> "11:00,58  F,58  F,100 %,WSW,22 mph,0 mph,29.6 in,0.0 in,0.0 in,Cloudy / Windy",
    12 -> "12:00,56  F,54  F,93 %,W,25 mph,36 mph,29.6 in,0.0 in,0.0 in,Cloudy / Windy",
    13 -> "13:00,55  F,52  F,89 %,W,23 mph,31 mph,29.7 in,0.0 in,0.0 in,Cloudy / Windy",
    14 -> "14:00 PM,63  F,63  F,100 %,SSW,22 mph,31 mph,29.6 in,0.0 in,0.0 in,Light Rain / Windy",
    15 -> "15:00 PM,62  F,62  F,100 %,NNW,16 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    16 -> "16:00 PM,60  F,60  F,100 %,N,14 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    17 -> "17:00 PM,59  F,58  F,96 %,NNE,7 mph,0 mph,29.6 in,0.0 in,0.0 in,Light Rain",
    18 -> "18:00 PM,58  F,58  F,100 %,NNE,7 mph,0 mph,29.6 in,0.1 in,0.0 in,Heavy Rain",
    19 -> "19:00 PM,59  F,59  F,100 %,ESE,7 mph,0 mph,29.6 in,0.1 in,0.0 in,Rain",
    20 -> "20:00 PM,60  F,60  F,100 %,ESE,5 mph,0 mph,29.6 in,0.0 in,0.0 in,Mostly Cloudy",
    21 -> "21:00 PM,61  F,61  F,100 %,S,14 mph,0 mph,29.6 in,0.0 in,0.3 in,Mostly Cloudy",
    22 -> "22:00 PM,60  F,60  F,100 %,WSW,20 mph,26 mph,29.6 in,0.0 in,0.0 in,Cloudy",
    23 -> "23:00 PM,58  F,58  F,100 %,WSW,22 mph,0 mph,29.6 in,0.0 in,0.0 in,Cloudy / Windy"
  )
  def generateDriverRecordLine (timeStamp : Long, driverNumber : Int): String = {
      val key = Utility.getRandomNumber(1,10)
      val driverId = "driver" + "_" + driverNumber
      val latitude = latitudeLongitudeMap.get(key).get.latitude
      val longitude = latitudeLongitudeMap.get(key).get.longitude
      val geohash = GeoHash
        .withCharacterPrecision(latitude, longitude, 6)
        .toBase32
      val driverRecordLine: String = Seq(timeStamp,driverId,latitude,longitude,geohash).mkString(",")
      (driverRecordLine)
  }
  def generatePassengerRecordLine (timeStamp : Long, passengerNumber : Int): String = {
    val key = Utility.getRandomNumber(1,10)
    val passengerId = "passenger" + "_" + passengerNumber
    val latitude = latitudeLongitudeMap.get(key).get.latitude
    val longitude = latitudeLongitudeMap.get(key).get.longitude
    val geohash = GeoHash
      .withCharacterPrecision(latitude, longitude, 6)
      .toBase32
    val passengerRecordLine = Seq(timeStamp,passengerId,latitude,longitude,geohash).mkString(",")
    (passengerRecordLine)
  }

  def generateWeatherRecordLine (geohash : String, timeStamp : Long): String = {
    // Geohash and timestamp can be used when connecting with a
    // weather api where current hour will be fetched by timestamp
    val  currentHour = new DateTime().getHourOfDay
    val weatherRecordLine = weatherDataMap.get(currentHour).get
    (weatherRecordLine)
  }
}
