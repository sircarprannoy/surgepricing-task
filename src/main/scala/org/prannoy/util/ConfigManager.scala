package org.prannoy.util

import com.typesafe.config.{Config, ConfigFactory}

object ConfigManager {
  val config : Config = ConfigFactory.load()

  def getString(key : String) = {
    config.getString(key)
  }
  def getInt(key : String) = {
    config.getInt(key)
  }
}
